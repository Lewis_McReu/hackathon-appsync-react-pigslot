import React, { Component } from "react";

export default class AddReservation extends Component {

    constructor(props) {
        super(props);
        this.state = this.getInitialState();
    }

    static defaultProps = {
        onAdd: () => null
    }

    getInitialState = () => ({
        amount: 0,
        user: '',
        location: '',
        date: '',
        hour: 6,
    });

    handleChange = (field, event) => {
        const { target: { value } } = event;

        this.setState({
            [field]: value
        });
    }

    handleAdd = () => {
        const { amount, user, location, date, hour } = this.state;

        this.setState(this.getInitialState(), () => {
            this.props.onAdd({ amount, user, location, date, hour });
        });
    }

    handleCancel = () => {
        this.setState(this.getInitialState());
    }

    render() {
        return (
            <fieldset >
                <legend>Add new Reservation</legend>
                <div>
                    <label>Amount<input type="number" placeholder="Amount" value={this.state.amount} onChange={this.handleChange.bind(this, 'amount')} /></label>
                </div>
                <div>
                    <label>User
                      <select>
                        <
                      </select>
                      <select type="text" placeholder="User" value={this.state.user} onChange={this.handleChange.bind(this, 'user')} /></label>
                </div>
                <div>
                    <label>Location<input type="text" placeholder="Location" value={this.state.location} onChange={this.handleChange.bind(this, 'location')} /></label>
                </div>
                <div>
                    <label>Date<input type="date" placeholder="Date" value={this.state.date} onChange={this.handleChange.bind(this, 'date')} /></label>
                </div>
                <div>
                    <label>Time<input type="number" placeholder="Time" value={this.state.time} onChange={this.handleChange.bind(this, 'time')} /></label>
                </div>
                <div>
                    <button onClick={this.handleAdd}>Add new reservation</button>
                    <button onClick={this.handleCancel}>Cancel</button>
                </div>
            </fieldset>
        );
    }
}
