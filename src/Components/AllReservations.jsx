import React, { Component } from "react";

export default class AllReservations extends Component {

    constructor(props) {
        super(props);

        this.state = {
        }
    }

    static defaultProps = {
        reservations: []
    }

    renderOrEditReservation = (reservation) => {
        return (
                    <tr key={reservation.id}>
                        <td>{reservation.amount}</td>
                        <td>{reservation.user.name}</td>
                        <td>{reservation.location.name}</td>
                        <td>{reservation.date}</td>
                        <td>{reservation.hour}</td>
                    </tr>
                );
    }

    render() {
        const { reservations } = this.props;

        return (<table width="100%">
            <thead>
                <tr>
                    <th>amount</th>
                    <th>user</th>
                    <th>location</th>
                    <th>date</th>
                    <th>hour</th>
                </tr>
            </thead>
            <tbody>
                {[].concat(reservations).sort((a, b) => b.id - a.id).map(this.renderOrEditReservation)}
            </tbody>
        </table>);
    }
}
