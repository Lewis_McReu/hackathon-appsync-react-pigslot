import React, { Component } from 'react';
import './App.css';

import AllReservations from "./Components/AllReservations";
import AddReservation from "./Components/AddReservation";

import AWSAppSyncClient from "aws-appsync";
import { Rehydrated } from 'aws-appsync-react';
import { AUTH_TYPE } from "aws-appsync/lib/link/auth-link";
import { graphql, ApolloProvider, compose } from 'react-apollo';
import AppSync from './AppSync.js';
import ListReservations from './Queries/ListReservationsQuery';
import CreateReservationMutation from './Mutations/CreateReservationMutation';
import CreateReservationSubscription from './Subscriptions/NewReservationSubscription';

const client = new AWSAppSyncClient({
    url: AppSync.graphqlEndpoint,
    region: AppSync.region,
    auth: {
        type: AUTH_TYPE.API_KEY,
        apiKey: AppSync.apiKey,
    },
});

class App extends Component {
    render() {
        return (
        <div className="App">
            <h1 className="App-title">Pigslots AppSync Demo</h1>
            <CreateReservationWithData />
            <AllReservationsWithData />
        </div>
        );
    }
}

const AllReservationsWithData = compose(
    graphql(ListReservations, {
        options: {
            fetchPolicy: 'cache-and-network'
        },
        props: (props) => ({
            reservations: props.data.listReservations && props.data.listReservations.items,
            subscribeToNewReservations: params => {
                props.data.subscribeToMore({
                    document: CreateReservationSubscription,
                    updateQuery: (prev, { subscriptionData: { data : { reservation } } }) => ({
                        ...prev,
                        listReservations: { posts: [reservation, ...prev.listReservations.items.filter(res => res.id !== reservation.id)] }
                    })
                });
            },
        })
    })
  )(AllReservations);

    const CreateReservationWithData = graphql(CreateReservationMutation, {
    props: (props) => ({
        onAdd: reservation => props.mutate({
            variables: reservation,
            optimisticResponse: () => ({ createReservation: { ...reservation } }),
        })
    }),
    options: {
        refetchQueries: [{ query: ListReservations }],
        update: (dataProxy, { data: { createReservationMutation } }) => {
            const query = ListReservations;
            const data = dataProxy.readQuery({ query });

            data.listReservations.items.push(createReservationMutation);

            dataProxy.writeQuery({ query, data });
        }
    }
})(AddReservation);

const WithProvider = () => (
    <ApolloProvider client={client}>
        <Rehydrated>
            <App />
        </Rehydrated>
    </ApolloProvider>
);

export default WithProvider;
