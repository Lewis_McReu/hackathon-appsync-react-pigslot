import gql from 'graphql-tag';

export default gql`
query ListLocations {
    getLocations {
        items{
          id
          name
          startHour
          endHour
          slotLimit
        }
    }
}`;
