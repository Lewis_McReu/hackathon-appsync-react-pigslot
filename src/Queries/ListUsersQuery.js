import gql from 'graphql-tag';

export default gql`
query ListUsers {
    getUsers {
        items{
          id
          name
          email
        }
    }
}`;
