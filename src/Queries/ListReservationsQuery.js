import gql from 'graphql-tag';

export default gql`
query ListReservations($locationId: String, $date: String) {
    getReservations(
      locationId: $locationId
      date: $date
    ){
        items{
          id
          amount
          user{
            id
            name
          }
          date
          hour
          location{
            id
            name
          }
        }
    }
}`;
