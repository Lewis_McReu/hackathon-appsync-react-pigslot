import gql from 'graphql-tag';

export default gql`
    subscription NewReservation($locationId: String, $date: String) {
    newReservation(
      locationId: $locationId
      date: $date
    ) {
        id
        amount
        user{ id }
        location{ id }
        date
        hour
    }
}`;
