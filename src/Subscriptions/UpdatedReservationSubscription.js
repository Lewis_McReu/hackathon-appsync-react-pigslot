import gql from 'graphql-tag';

export default gql`
    subscription UpdatedReservation($locationId: String, $date: String) {
    updatedReservation(
      locationId: $locationId
      date: $date
    ) {
        id
        amount
    }
}`;
