import gql from 'graphql-tag';

export default gql`
    subscription DeletedReservation($id: String, $locationId: String, $date: String) {
    updatedReservation(
      id: $id
      locationId: $locationId
      date: $date
    ) {
        id
    }
}`;
