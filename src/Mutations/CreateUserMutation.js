import gql from 'graphql-tag';

export default gql`
  mutation CreateUser($name: String!, $email: String!) {
    createUser(
      name: $name
      email: $email
    ){
      id
      name
      email
    }
  }
`;
