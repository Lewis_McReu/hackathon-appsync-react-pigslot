import gql from 'graphql-tag';

export default gql`
  mutation CreateReservation($userId: String!, $locationId: String!, $amount: Int!, $date: String!, $hour: String!) {
    createReservation(
      userId: $userId
      locationId: $locationId
      amount: $amount
      date: $date
      hour: $hour
    ){
      id
      user{
        id
        name
      }
      location {
        id
        name
      }
      amount
      date
      hour
    }
  }
`;
