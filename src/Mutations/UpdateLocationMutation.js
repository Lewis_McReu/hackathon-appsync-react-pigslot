import gql from 'graphql-tag';

export default gql`
  mutation UpdateLocation($id: ID!, $name: String, $startHour: Int, $endHour: Int, $slotLimit: Int) {
    updateLocation(
      id: $id
      name: $name
      startHour: $startHour
      endHour: $endHour
      slotLimit: $slotLimit
    ){
      id
      name
      startHour
      endHour
      slotLimit
    }
  }
`;
