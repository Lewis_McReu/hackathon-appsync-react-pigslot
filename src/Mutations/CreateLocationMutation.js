import gql from 'graphql-tag';

export default gql`
  mutation CreateLocation($name: String!, $startHour: Int!, $endHour: Int!, $slotLimit: Int!) {
    createLocation(
      name: $name
      startHour: $startHour
      endHour: $endHour
      slotLimit: $slotLimit
    ){
      id
      name
      startHour
      endHour
      slotLimit
    }
  }
`;
