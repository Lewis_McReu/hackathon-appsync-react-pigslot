import gql from 'graphql-tag';

export default gql`
  mutation UpdateReservation($id: ID!, $amount: Int!) {
    updateReservation(
      id: $id
      amount: $amount
    ){
      id
      amount
    }
  }
`;
